#include <cstdio>
#include <cmath>
#include <iostream>
using namespace std;

int gcd(int a, int b)
{
    int c;
    while(a!=0){
        c=b%a;
        b=a;
        a=c;
    }
    return b;
}

int main()
{
    int n, g=0, i, j;

    while(scanf("%d", &n), n!=0){
        for(i=1;i<n;i++)
            for(j=i+1;j<=n;j++){
                g+=gcd(i,j);
            }
            printf("%d\n", g);
            g=0;
    }

    return 0;
}
